import { Component, OnInit, Injector, } from '@angular/core';
import {
    DictionaryValueServiceProxy,
    CreateOrUpdateDictionaryValueInput,
    DictionaryValueListDto
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ModalComponentBase } from '@shared/common/modal-component-base';

export interface IDictionaryValueOnEdit {
    id?: number;
    itemId?: number;
    name?: string;
    dValue?: string;
    sortCode?: number;
    itemName?: string;
}

@Component({
    selector: 'createOrEditDictionaryValueModal',
    templateUrl: './create-or-edit-dictionary-value-modal.component.html',
    styles: [],
})
export class CreateOrEditDictionaryValueModalComponent extends ModalComponentBase implements OnInit {

    dictionaryValue: IDictionaryValueOnEdit = {};
    saving = false;

    constructor(
        injector: Injector,
        private DictionaryValueService: DictionaryValueServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() { }

    save(): void {
        this.createOrUpdateDictionaryValue();
    }

    createOrUpdateDictionaryValue(): any {
        const updateInput = new CreateOrUpdateDictionaryValueInput();
        updateInput.dictionaryValue.id = this.dictionaryValue.id;
        updateInput.dictionaryValue.name = this.dictionaryValue.name;
        updateInput.dictionaryValue.dictionaryItemId = this.dictionaryValue.itemId;
        updateInput.dictionaryValue.dValue = this.dictionaryValue.dValue;
        updateInput.dictionaryValue.sortCode = this.dictionaryValue.sortCode;

        this.saving = true;
        this.DictionaryValueService
            .createOrUpdate(updateInput)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result: DictionaryValueListDto) => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.success(result);
            });
    }
}
