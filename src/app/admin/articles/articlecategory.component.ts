﻿import { Component, Injector } from '@angular/core';
import {
    PagedListingComponentBase,
    PagedRequestDto,
} from '@shared/common/paged-listing-component-base';
import {
    ArticleCategoryListDto,
    ArticleCategoryServiceProxy,
    PagedResultDtoOfArticleCategoryListDto
} from '@shared/service-proxies/service-proxies';
import { CreateOrEditArticleCategoryModalComponent } from './create-or-edit-articlecategory-modal.component';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-articlecategorys',
    templateUrl: './articlecategory.component.html',
    styles: [],
})
export class ArticleCategoryComponent extends PagedListingComponentBase<ArticleCategoryListDto> {
    advancedFiltersVisible = false;

    constructor(injector: Injector, private _articleCategoryService: ArticleCategoryServiceProxy) {
        super(injector);
    }

    protected fetchDataList(
        request: PagedRequestDto,
        pageNumber: number,
        finishedCallback: () => void,
    ): void {
        this._articleCategoryService.getArticleCategorys(
            this.filterText,
            request.sorting,
            request.maxResultCount,
            request.skipCount,
        )
        .pipe(finalize(finishedCallback))
        .subscribe((result: PagedResultDtoOfArticleCategoryListDto) => {
            this.dataList = result.items;
            this.showPaging(result);
        });
    }

    protected delete(entity: ArticleCategoryListDto): void {
        this._articleCategoryService.delete(entity.id).subscribe(() => {
            this.refresh();
            this.notify.success(this.l('SuccessfullyDeleted'));
        });
    }

    createOrEdit(id?: number): void {
        this.modalHelper
            .createStatic(CreateOrEditArticleCategoryModalComponent, { articleCategoryId: id }, {size: 'md', includeTabs: true })
            .subscribe(res => {
                if (res) {
                    this.refresh();
                }
            });
    }

    batchDelete(): void {
        this.message.warn('method not implement!');
        // const selectCount = this.selectedDataItems.length;
        // if (selectCount <= 0) {
        //     abp.message.warn(this.l('SelectAnItem'));

        //     return;
        // }
        // this.message.confirm(
        //     this.l('<b class="text-red">{0}</b> items will be removed.', selectCount),
        //     this.l('AreYouSure'),
        //     res => {
        //         if (res) {
        //             let deletedIds = _.map(this.selectedDataItems, (item) => new EntityDto({ id: item.id }));
        //             this._articleCategoryService.batchDeleteArticleCategorys(deletedIds).subscribe(() => {
        //                 this.refresh();
        //                 this.notify.success(this.l('SuccessfullyDeleted'));
        //             });
        //         }
        //     },
        // );
    }
}
